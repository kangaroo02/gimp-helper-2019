#ifndef __GIT_VERSION_H__
#define __GIT_VERSION_H__
#define GIMP_GIT_VERSION "Unknown, shouldn't happen"
#define GIMP_GIT_LAST_COMMIT_YEAR "2019"
#endif /* __GIT_VERSION_H__ */
