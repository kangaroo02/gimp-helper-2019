/* GIMP - The GNU Image Manipulation Program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * GimpTextTool
 * Copyright (C) 2002-2010  Sven Neumann <sven@gimp.org>
 *                          Daniel Eddeland <danedde@svn.gnome.org>
 *                          Michael Natterer <mitch@gimp.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"

#include <gegl.h>
#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>

#include "libgimpwidgets/gimpwidgets.h"

#include "tools-types.h"

#include "core/gimp.h"
#include "core/gimpimage.h"
#include "core/gimptoolinfo.h"

#include "text/gimptext.h"
#include "text/gimptextlayout.h"

#include "widgets/gimpdialogfactory.h"
#include "widgets/gimpoverlaybox.h"
#include "widgets/gimpoverlayframe.h"
#include "widgets/gimptextbuffer.h"
#include "widgets/gimptexteditor.h"
#include "widgets/gimptextproxy.h"
#include "widgets/gimptextstyleeditor.h"

#include "display/gimpdisplay.h"
#include "display/gimpdisplayshell.h"
#include "display/gimpdisplayshell-transform.h"

#include "gimprectangletool.h"
#include "gimptextoptions.h"
#include "gimptexttool.h"
#include "gimptexttool-editor.h"

#include "gimp-log.h"
#include "gimp-intl.h"


/*  local function prototypes  */

static void   gimp_helper_ensure_proxy       (GimpTextTool    *helper);
static void   gimp_helper_move_cursor        (GimpTextTool    *helper,
                                                 GtkMovementStep  step,
                                                 gint             count,
                                                 gboolean         extend_selection);
static void   gimp_helper_insert_at_cursor   (GimpTextTool    *helper,
                                                 const gchar     *str);
static void   gimp_helper_delete_from_cursor (GimpTextTool    *helper,
                                                 GtkDeleteType    type,
                                                 gint             count);
static void   gimp_helper_backspace          (GimpTextTool    *helper);
static void   gimp_helper_toggle_overwrite   (GimpTextTool    *helper);
static void   gimp_helper_select_all         (GimpTextTool    *helper,
                                                 gboolean         select);
static void   gimp_helper_change_size        (GimpTextTool    *helper,
                                                 gdouble          amount);
static void   gimp_helper_change_baseline    (GimpTextTool    *helper,
                                                 gdouble          amount);
static void   gimp_helper_change_kerning     (GimpTextTool    *helper,
                                                 gdouble          amount);

static void   gimp_helper_options_notify     (GimpTextOptions *options,
                                                 GParamSpec      *pspec,
                                                 GimpTextTool    *helper);
static void   gimp_helper_editor_dialog      (GimpTextTool    *helper);
static void   gimp_helper_editor_destroy     (GtkWidget       *dialog,
                                                 GimpTextTool    *helper);
static void   gimp_helper_enter_text         (GimpTextTool    *helper,
                                                 const gchar     *str);
static void   gimp_helper_xy_to_iter         (GimpTextTool    *helper,
                                                 gdouble          x,
                                                 gdouble          y,
                                                 GtkTextIter     *iter);
static void   gimp_helper_im_commit          (GtkIMContext    *context,
                                                 const gchar     *str,
                                                 GimpTextTool    *helper);
static void   gimp_helper_im_preedit_start   (GtkIMContext    *context,
                                                 GimpTextTool    *helper);
static void   gimp_helper_im_preedit_end     (GtkIMContext    *context,
                                                 GimpTextTool    *helper);
static void   gimp_helper_im_preedit_changed (GtkIMContext    *context,
                                                 GimpTextTool    *helper);


/*  public functions  */

void
gimp_helper_editor_init (GimpTextTool *helper)
{
  helper->im_context     = gtk_im_multicontext_new ();
  helper->needs_im_reset = FALSE;

  helper->preedit_string = NULL;
  helper->preedit_cursor = 0;
  helper->overwrite_mode = FALSE;
  helper->x_pos          = -1;

  g_signal_connect (helper->im_context, "commit",
                    G_CALLBACK (gimp_helper_im_commit),
                    helper);
  g_signal_connect (helper->im_context, "preedit-start",
                    G_CALLBACK (gimp_helper_im_preedit_start),
                    helper);
  g_signal_connect (helper->im_context, "preedit-end",
                    G_CALLBACK (gimp_helper_im_preedit_end),
                    helper);
  g_signal_connect (helper->im_context, "preedit-changed",
                    G_CALLBACK (gimp_helper_im_preedit_changed),
                    helper);

}

void
gimp_helper_editor_finalize (GimpTextTool *helper)
{
  if (helper->im_context)
    {
      g_object_unref (helper->im_context);
      helper->im_context = NULL;
    }
}

void
gimp_helper_editor_start (GimpTextTool *helper)
{
  GimpTool         *tool    = GIMP_TOOL (helper);
  GimpTextOptions  *options = GIMP_helper_GET_OPTIONS (helper);
  GimpDisplayShell *shell   = gimp_display_get_shell (tool->display);

  gtk_im_context_set_client_window (helper->im_context,
                                    gtk_widget_get_window (shell->canvas));

  helper->needs_im_reset = TRUE;
  gimp_helper_reset_im_context (helper);

  gtk_im_context_focus_in (helper->im_context);

  if (options->use_editor)
    gimp_helper_editor_dialog (helper);

  g_signal_connect (options, "notify::use-editor",
                    G_CALLBACK (gimp_helper_options_notify),
                    helper);

  if (! helper->style_overlay)
    {
      Gimp    *gimp = GIMP_CONTEXT (options)->gimp;
      gdouble  xres = 1.0;
      gdouble  yres = 1.0;

      helper->style_overlay = gimp_overlay_frame_new ();
      gtk_container_set_border_width (GTK_CONTAINER (helper->style_overlay),
                                      4);
      gimp_display_shell_add_overlay (shell,
                                      helper->style_overlay,
                                      0, 0,
                                      GIMP_HANDLE_ANCHOR_CENTER, 0, 0);
      gimp_overlay_box_set_child_opacity (GIMP_OVERLAY_BOX (shell->canvas),
                                          helper->style_overlay, 0.7);

      if (helper->image)
        gimp_image_get_resolution (helper->image, &xres, &yres);

      helper->style_editor = gimp_text_style_editor_new (gimp,
                                                            helper->proxy,
                                                            helper->buffer,
                                                            gimp->fonts,
                                                            xres, yres);
      gtk_container_add (GTK_CONTAINER (helper->style_overlay),
                         helper->style_editor);
      gtk_widget_show (helper->style_editor);
    }

  gimp_helper_editor_position (helper);
  gtk_widget_show (helper->style_overlay);
}

void
gimp_helper_editor_position (GimpTextTool *helper)
{
  if (helper->style_overlay)
    {
      GimpTool         *tool    = GIMP_TOOL (helper);
      GimpDisplayShell *shell   = gimp_display_get_shell (tool->display);
      GtkRequisition    requisition;
      gint              x, y;

      gtk_widget_size_request (helper->style_overlay, &requisition);

      g_object_get (helper,
                    "x1", &x,
                    "y1", &y,
                    NULL);

      gimp_display_shell_move_overlay (shell,
                                       helper->style_overlay,
                                       x, y,
                                       GIMP_HANDLE_ANCHOR_SOUTH_WEST, 4, 12);

      if (helper->image)
        {
          gdouble xres, yres;

          gimp_image_get_resolution (helper->image, &xres, &yres);

          g_object_set (helper->style_editor,
                        "resolution-x", xres,
                        "resolution-y", yres,
                        NULL);
        }
    }
}

void
gimp_helper_editor_halt (GimpTextTool *helper)
{
  GimpTextOptions *options = GIMP_helper_GET_OPTIONS (helper);

  if (helper->style_overlay)
    {
      gtk_widget_destroy (helper->style_overlay);
      helper->style_overlay = NULL;
      helper->style_editor  = NULL;
    }

  g_signal_handlers_disconnect_by_func (options,
                                        gimp_helper_options_notify,
                                        helper);

  if (helper->editor_dialog)
    {
      g_signal_handlers_disconnect_by_func (helper->editor_dialog,
                                            gimp_helper_editor_destroy,
                                            helper);
      gtk_widget_destroy (helper->editor_dialog);
    }

  if (helper->proxy_text_view)
    {
      gtk_widget_destroy (helper->offscreen_window);
      helper->offscreen_window = NULL;
      helper->proxy_text_view = NULL;
    }

  helper->needs_im_reset = TRUE;
  gimp_helper_reset_im_context (helper);

  gtk_im_context_focus_out (helper->im_context);

  gtk_im_context_set_client_window (helper->im_context, NULL);
}

void
gimp_helper_editor_button_press (GimpTextTool        *helper,
                                    gdouble              x,
                                    gdouble              y,
                                    GimpButtonPressType  press_type)
{
  GtkTextBuffer *buffer = GTK_TEXT_BUFFER (helper->buffer);
  GtkTextIter    cursor;
  GtkTextIter    selection;

  gimp_helper_xy_to_iter (helper, x, y, &cursor);

  selection = cursor;

  helper->select_start_iter = cursor;
  helper->select_words      = FALSE;
  helper->select_lines      = FALSE;

  switch (press_type)
    {
    case GIMP_BUTTON_PRESS_NORMAL:
      gtk_text_buffer_place_cursor (buffer, &cursor);
      break;

    case GIMP_BUTTON_PRESS_DOUBLE:
      helper->select_words = TRUE;

      if (! gtk_text_iter_starts_word (&cursor))
        gtk_text_iter_backward_visible_word_starts (&cursor, 1);

      if (! gtk_text_iter_ends_word (&selection) &&
          ! gtk_text_iter_forward_visible_word_ends (&selection, 1))
        gtk_text_iter_forward_to_line_end (&selection);

      gtk_text_buffer_select_range (buffer, &cursor, &selection);
      break;

    case GIMP_BUTTON_PRESS_TRIPLE:
      helper->select_lines = TRUE;

      gtk_text_iter_set_line_offset (&cursor, 0);
      gtk_text_iter_forward_to_line_end (&selection);

      gtk_text_buffer_select_range (buffer, &cursor, &selection);
      break;
    }
}

void
gimp_helper_editor_button_release (GimpTextTool *helper)
{
  GtkTextBuffer *buffer = GTK_TEXT_BUFFER (helper->buffer);

  if (gtk_text_buffer_get_has_selection (buffer))
    {
      GimpTool         *tool  = GIMP_TOOL (helper);
      GimpDisplayShell *shell = gimp_display_get_shell (tool->display);
      GtkClipboard     *clipboard;

      clipboard = gtk_widget_get_clipboard (GTK_WIDGET (shell),
                                            GDK_SELECTION_PRIMARY);

      gtk_text_buffer_copy_clipboard (buffer, clipboard);
    }
}

void
gimp_helper_editor_motion (GimpTextTool *helper,
                              gdouble       x,
                              gdouble       y)
{
  GtkTextBuffer *buffer = GTK_TEXT_BUFFER (helper->buffer);
  GtkTextIter    old_cursor;
  GtkTextIter    old_selection;
  GtkTextIter    cursor;
  GtkTextIter    selection;

  gtk_text_buffer_get_iter_at_mark (buffer, &old_cursor,
                                    gtk_text_buffer_get_insert (buffer));
  gtk_text_buffer_get_iter_at_mark (buffer, &old_selection,
                                    gtk_text_buffer_get_selection_bound (buffer));

  gimp_helper_xy_to_iter (helper, x, y, &cursor);
  selection = helper->select_start_iter;

  if (helper->select_words ||
      helper->select_lines)
    {
      GtkTextIter start;
      GtkTextIter end;

      if (gtk_text_iter_compare (&cursor, &selection) < 0)
        {
          start = cursor;
          end   = selection;
        }
      else
        {
          start = selection;
          end   = cursor;
        }

      if (helper->select_words)
        {
          if (! gtk_text_iter_starts_word (&start))
            gtk_text_iter_backward_visible_word_starts (&start, 1);

          if (! gtk_text_iter_ends_word (&end) &&
              ! gtk_text_iter_forward_visible_word_ends (&end, 1))
            gtk_text_iter_forward_to_line_end (&end);
        }
      else if (helper->select_lines)
        {
          gtk_text_iter_set_line_offset (&start, 0);
          gtk_text_iter_forward_to_line_end (&end);
        }

      if (gtk_text_iter_compare (&cursor, &selection) < 0)
        {
          cursor    = start;
          selection = end;
        }
      else
        {
          selection = start;
          cursor    = end;
        }
    }

  if (! gtk_text_iter_equal (&cursor,    &old_cursor) ||
      ! gtk_text_iter_equal (&selection, &old_selection))
    {
      gimp_draw_tool_pause (GIMP_DRAW_TOOL (helper));

      gtk_text_buffer_select_range (buffer, &cursor, &selection);

      gimp_draw_tool_resume (GIMP_DRAW_TOOL (helper));
    }
}

gboolean
gimp_helper_editor_key_press (GimpTextTool *helper,
                                 GdkEventKey  *kevent)
{
  GimpTool         *tool   = GIMP_TOOL (helper);
  GimpDisplayShell *shell  = gimp_display_get_shell (tool->display);
  GtkTextBuffer    *buffer = GTK_TEXT_BUFFER (helper->buffer);
  GtkTextIter       cursor;
  GtkTextIter       selection;
  gboolean          retval = TRUE;

  if (! gtk_widget_has_focus (shell->canvas))
    {
      /*  The focus is in the floating style editor, and the event
       *  was not handled there, focus the canvas.
       */
      switch (kevent->keyval)
        {
        case GDK_KEY_Tab:
        case GDK_KEY_KP_Tab:
        case GDK_KEY_ISO_Left_Tab:
        case GDK_KEY_Escape:
          gtk_widget_grab_focus (shell->canvas);
          return TRUE;

        default:
          break;
        }
    }

  if (gtk_im_context_filter_keypress (helper->im_context, kevent))
    {
      helper->needs_im_reset = TRUE;
      helper->x_pos          = -1;

      return TRUE;
    }

  gimp_helper_ensure_proxy (helper);

  if (gtk_bindings_activate_event (GTK_OBJECT (helper->proxy_text_view),
                                   kevent))
    {
      GIMP_LOG (TEXT_EDITING, "binding handled event");

      return TRUE;
    }

  gtk_text_buffer_get_iter_at_mark (buffer, &cursor,
                                    gtk_text_buffer_get_insert (buffer));
  gtk_text_buffer_get_iter_at_mark (buffer, &selection,
                                    gtk_text_buffer_get_selection_bound (buffer));

  switch (kevent->keyval)
    {
    case GDK_KEY_Return:
    case GDK_KEY_KP_Enter:
    case GDK_KEY_ISO_Enter:
      gimp_helper_reset_im_context (helper);
      gimp_helper_enter_text (helper, "\n");
      break;

    case GDK_KEY_Tab:
    case GDK_KEY_KP_Tab:
    case GDK_KEY_ISO_Left_Tab:
      gimp_helper_reset_im_context (helper);
      gimp_helper_enter_text (helper, "\t");
      break;

    case GDK_KEY_Escape:
      gimp_rectangle_tool_cancel (GIMP_RECTANGLE_TOOL (helper));
      gimp_tool_control (GIMP_TOOL (helper), GIMP_TOOL_ACTION_HALT,
                         GIMP_TOOL (helper)->display);
      break;

    default:
      retval = FALSE;
    }

  helper->x_pos = -1;

  return retval;
}

gboolean
gimp_helper_editor_key_release (GimpTextTool *helper,
                                   GdkEventKey  *kevent)
{
  if (gtk_im_context_filter_keypress (helper->im_context, kevent))
    {
      helper->needs_im_reset = TRUE;

      return TRUE;
    }

  gimp_helper_ensure_proxy (helper);

  if (gtk_bindings_activate_event (GTK_OBJECT (helper->proxy_text_view),
                                   kevent))
    {
      GIMP_LOG (TEXT_EDITING, "binding handled event");

      return TRUE;
    }

  return FALSE;
}

void
gimp_helper_reset_im_context (GimpTextTool *helper)
{
  if (helper->needs_im_reset)
    {
      helper->needs_im_reset = FALSE;
      gtk_im_context_reset (helper->im_context);
    }
}

void
gimp_helper_editor_get_cursor_rect (GimpTextTool   *helper,
                                       gboolean        overwrite,
                                       PangoRectangle *cursor_rect)
{
  GtkTextBuffer *buffer = GTK_TEXT_BUFFER (helper->buffer);
  PangoLayout   *layout;
  gint           offset_x;
  gint           offset_y;
  GtkTextIter    cursor;
  gint           cursor_index;

  g_return_if_fail (GIMP_IS_helper (helper));
  g_return_if_fail (cursor_rect != NULL);

  gtk_text_buffer_get_iter_at_mark (buffer, &cursor,
                                    gtk_text_buffer_get_insert (buffer));
  cursor_index = gimp_text_buffer_get_iter_index (helper->buffer, &cursor,
                                                  TRUE);

  gimp_helper_ensure_layout (helper);

  layout = gimp_text_layout_get_pango_layout (helper->layout);

  gimp_text_layout_get_offsets (helper->layout, &offset_x, &offset_y);

  if (overwrite)
    pango_layout_index_to_pos (layout, cursor_index, cursor_rect);
  else
    pango_layout_get_cursor_pos (layout, cursor_index, cursor_rect, NULL);

  gimp_text_layout_transform_rect (helper->layout, cursor_rect);

  cursor_rect->x      = PANGO_PIXELS (cursor_rect->x) + offset_x;
  cursor_rect->y      = PANGO_PIXELS (cursor_rect->y) + offset_y;
  cursor_rect->width  = PANGO_PIXELS (cursor_rect->width);
  cursor_rect->height = PANGO_PIXELS (cursor_rect->height);
}

void
gimp_helper_editor_update_im_rect (GimpTextTool *helper)
{
  GimpDisplayShell *shell;
  PangoRectangle    rect = { 0, };
  gint              off_x, off_y;

  g_return_if_fail (GIMP_IS_helper (helper));

  shell = gimp_display_get_shell (GIMP_TOOL (helper)->display);

  if (helper->text)
    gimp_helper_editor_get_cursor_rect (helper,
                                           helper->overwrite_mode,
                                           &rect);

  g_object_get (helper, "x1", &off_x, "y1", &off_y, NULL);
  rect.x += off_x;
  rect.y += off_y;

  gimp_display_shell_transform_xy (shell, rect.x, rect.y, &rect.x, &rect.y);

  if (helper->preedit_overlay)
    {
      GtkRequisition requisition;

      gtk_widget_size_request (helper->preedit_overlay, &requisition);

      rect.width  = requisition.width;
      rect.height = requisition.height;
    }

  gtk_im_context_set_cursor_location (helper->im_context,
                                      (GdkRectangle *) &rect);
}


/*  private functions  */

static void
gimp_helper_ensure_proxy (GimpTextTool *helper)
{
  GimpTool         *tool  = GIMP_TOOL (helper);
  GimpDisplayShell *shell = gimp_display_get_shell (tool->display);

  if (helper->offscreen_window &&
      gtk_widget_get_screen (helper->offscreen_window) !=
      gtk_widget_get_screen (GTK_WIDGET (shell)))
    {
      gtk_window_set_screen (GTK_WINDOW (helper->offscreen_window),
                             gtk_widget_get_screen (GTK_WIDGET (shell)));
      gtk_window_move (GTK_WINDOW (helper->offscreen_window), -200, -200);
    }
  else if (! helper->offscreen_window)
    {
      helper->offscreen_window = gtk_window_new (GTK_WINDOW_POPUP);
      gtk_window_set_screen (GTK_WINDOW (helper->offscreen_window),
                             gtk_widget_get_screen (GTK_WIDGET (shell)));
      gtk_window_move (GTK_WINDOW (helper->offscreen_window), -200, -200);
      gtk_widget_show (helper->offscreen_window);

      helper->proxy_text_view = gimp_text_proxy_new ();
      gtk_container_add (GTK_CONTAINER (helper->offscreen_window),
                         helper->proxy_text_view);
      gtk_widget_show (helper->proxy_text_view);

      g_signal_connect_swapped (helper->proxy_text_view, "move-cursor",
                                G_CALLBACK (gimp_helper_move_cursor),
                                helper);
      g_signal_connect_swapped (helper->proxy_text_view, "insert-at-cursor",
                                G_CALLBACK (gimp_helper_insert_at_cursor),
                                helper);
      g_signal_connect_swapped (helper->proxy_text_view, "delete-from-cursor",
                                G_CALLBACK (gimp_helper_delete_from_cursor),
                                helper);
      g_signal_connect_swapped (helper->proxy_text_view, "backspace",
                                G_CALLBACK (gimp_helper_backspace),
                                helper);
      g_signal_connect_swapped (helper->proxy_text_view, "cut-clipboard",
                                G_CALLBACK (gimp_helper_cut_clipboard),
                                helper);
      g_signal_connect_swapped (helper->proxy_text_view, "copy-clipboard",
                                G_CALLBACK (gimp_helper_copy_clipboard),
                                helper);
      g_signal_connect_swapped (helper->proxy_text_view, "paste-clipboard",
                                G_CALLBACK (gimp_helper_paste_clipboard),
                                helper);
      g_signal_connect_swapped (helper->proxy_text_view, "toggle-overwrite",
                                G_CALLBACK (gimp_helper_toggle_overwrite),
                                helper);
      g_signal_connect_swapped (helper->proxy_text_view, "select-all",
                                G_CALLBACK (gimp_helper_select_all),
                                helper);
      g_signal_connect_swapped (helper->proxy_text_view, "change-size",
                                G_CALLBACK (gimp_helper_change_size),
                                helper);
      g_signal_connect_swapped (helper->proxy_text_view, "change-baseline",
                                G_CALLBACK (gimp_helper_change_baseline),
                                helper);
      g_signal_connect_swapped (helper->proxy_text_view, "change-kerning",
                                G_CALLBACK (gimp_helper_change_kerning),
                                helper);
    }
}

static void
gimp_helper_move_cursor (GimpTextTool    *helper,
                            GtkMovementStep  step,
                            gint             count,
                            gboolean         extend_selection)
{
  GtkTextBuffer *buffer = GTK_TEXT_BUFFER (helper->buffer);
  GtkTextIter    cursor;
  GtkTextIter    selection;
  GtkTextIter   *sel_start;
  gboolean       cancel_selection = FALSE;
  gint           x_pos  = -1;

  GIMP_LOG (TEXT_EDITING, "%s count = %d, select = %s",
            g_enum_get_value (g_type_class_ref (GTK_TYPE_MOVEMENT_STEP),
                              step)->value_name,
            count,
            extend_selection ? "TRUE" : "FALSE");

  gtk_text_buffer_get_iter_at_mark (buffer, &cursor,
                                    gtk_text_buffer_get_insert (buffer));
  gtk_text_buffer_get_iter_at_mark (buffer, &selection,
                                    gtk_text_buffer_get_selection_bound (buffer));

  if (extend_selection)
    {
      sel_start = &selection;
    }
  else
    {
      /*  when there is a selection, moving the cursor without
       *  extending it should move the cursor to the end of the
       *  selection that is in moving direction
       */
      if (count > 0)
        gtk_text_iter_order (&selection, &cursor);
      else
        gtk_text_iter_order (&cursor, &selection);

      sel_start = &cursor;

      /* if we actually have a selection, just move *to* the beginning/end
       * of the selection and not *from* there on LOGICAL_POSITIONS
       * and VISUAL_POSITIONS movement
       */
      if (! gtk_text_iter_equal (&cursor, &selection))
        cancel_selection = TRUE;
    }

  switch (step)
    {
    case GTK_MOVEMENT_LOGICAL_POSITIONS:
      if (! cancel_selection)
        gtk_text_iter_forward_visible_cursor_positions (&cursor, count);
      break;

    case GTK_MOVEMENT_VISUAL_POSITIONS:
      if (! cancel_selection)
        {
          PangoLayout *layout;
          const gchar *text;

          if (! gimp_helper_ensure_layout (helper))
            break;

          layout = gimp_text_layout_get_pango_layout (helper->layout);
          text = pango_layout_get_text (layout);

          while (count != 0)
            {
              const gunichar word_joiner = 8288; /*g_utf8_get_char(WORD_JOINER);*/
              gint index;
              gint trailing = 0;
              gint new_index;

              index = gimp_text_buffer_get_iter_index (helper->buffer,
                                                       &cursor, TRUE);

              if (count > 0)
                {
                  if (g_utf8_get_char (text + index) == word_joiner)
                    pango_layout_move_cursor_visually (layout, TRUE,
                                                       index, 0, 1,
                                                       &new_index, &trailing);
                  else
                    new_index = index;

                  pango_layout_move_cursor_visually (layout, TRUE,
                                                     new_index, trailing, 1,
                                                     &new_index, &trailing);
                  count--;
                }
              else
                {
                  pango_layout_move_cursor_visually (layout, TRUE,
                                                     index, 0, -1,
                                                     &new_index, &trailing);

                  if (new_index != -1 && new_index != G_MAXINT &&
                      g_utf8_get_char (text + new_index) == word_joiner)
                    {
                      pango_layout_move_cursor_visually (layout, TRUE,
                                                         new_index, trailing, -1,
                                                         &new_index, &trailing);
                    }

                  count++;
                }

              if (new_index != G_MAXINT && new_index != -1)
                index = new_index;
              else
                break;

              gimp_text_buffer_get_iter_at_index (helper->buffer,
                                                  &cursor, index, TRUE);
              gtk_text_iter_forward_chars (&cursor, trailing);
            }
        }
      break;

    case GTK_MOVEMENT_WORDS:
      if (count < 0)
        {
          gtk_text_iter_backward_visible_word_starts (&cursor, -count);
        }
      else if (count > 0)
        {
	  if (! gtk_text_iter_forward_visible_word_ends (&cursor, count))
	    gtk_text_iter_forward_to_line_end (&cursor);
        }
      break;

    case GTK_MOVEMENT_DISPLAY_LINES:
      {
        GtkTextIter      start;
        GtkTextIter      end;
        gint             cursor_index;
        PangoLayout     *layout;
        PangoLayoutLine *layout_line;
        PangoLayoutIter *layout_iter;
        PangoRectangle   logical;
        gint             line;
        gint             trailing;
        gint             i;

        gtk_text_buffer_get_bounds (buffer, &start, &end);

        cursor_index = gimp_text_buffer_get_iter_index (helper->buffer,
                                                        &cursor, TRUE);

        if (! gimp_helper_ensure_layout (helper))
          break;

        layout = gimp_text_layout_get_pango_layout (helper->layout);

        pango_layout_index_to_line_x (layout, cursor_index, FALSE,
                                      &line, &x_pos);

        layout_iter = pango_layout_get_iter (layout);
        for (i = 0; i < line; i++)
          pango_layout_iter_next_line (layout_iter);

        pango_layout_iter_get_line_extents (layout_iter, NULL, &logical);

        x_pos += logical.x;

        pango_layout_iter_free (layout_iter);

        /*  try to go to the remembered x_pos if it exists *and* we are at
         *  the beginning or at the end of the current line
         */
        if (helper->x_pos != -1 && (x_pos <= logical.x ||
                                       x_pos >= logical.x + logical.width))
          x_pos = helper->x_pos;

        line += count;

        if (line < 0)
          {
            cursor = start;
            break;
          }
        else if (line >= pango_layout_get_line_count (layout))
          {
            cursor = end;
            break;
          }

        layout_iter = pango_layout_get_iter (layout);
        for (i = 0; i < line; i++)
          pango_layout_iter_next_line (layout_iter);

        layout_line = pango_layout_iter_get_line_readonly (layout_iter);
        pango_layout_iter_get_line_extents (layout_iter, NULL, &logical);

        pango_layout_iter_free (layout_iter);

        pango_layout_line_x_to_index (layout_line, x_pos - logical.x,
                                      &cursor_index, &trailing);

        gimp_text_buffer_get_iter_at_index (helper->buffer, &cursor,
                                            cursor_index, TRUE);

        while (trailing--)
          gtk_text_iter_forward_char (&cursor);
      }
      break;

    case GTK_MOVEMENT_PAGES: /* well... */
    case GTK_MOVEMENT_BUFFER_ENDS:
      if (count < 0)
        {
          gtk_text_buffer_get_start_iter (buffer, &cursor);
        }
      else if (count > 0)
        {
          gtk_text_buffer_get_end_iter (buffer, &cursor);
        }
      break;

    case GTK_MOVEMENT_PARAGRAPH_ENDS:
      if (count < 0)
        {
          gtk_text_iter_set_line_offset (&cursor, 0);
        }
      else if (count > 0)
        {
          if (! gtk_text_iter_ends_line (&cursor))
            gtk_text_iter_forward_to_line_end (&cursor);
        }
      break;

    case GTK_MOVEMENT_DISPLAY_LINE_ENDS:
      if (count < 0)
        {
          gtk_text_iter_set_line_offset (&cursor, 0);
        }
      else if (count > 0)
        {
          if (! gtk_text_iter_ends_line (&cursor))
            gtk_text_iter_forward_to_line_end (&cursor);
        }
      break;

    default:
      return;
    }

  helper->x_pos = x_pos;

  gimp_draw_tool_pause (GIMP_DRAW_TOOL (helper));

  gimp_helper_reset_im_context (helper);

  gtk_text_buffer_select_range (buffer, &cursor, sel_start);

  gimp_draw_tool_resume (GIMP_DRAW_TOOL (helper));
}

static void
gimp_helper_insert_at_cursor (GimpTextTool *helper,
                                 const gchar  *str)
{
  gimp_text_buffer_insert (helper->buffer, str);
}

static gboolean
is_whitespace (gunichar ch,
               gpointer user_data)
{
  return (ch == ' ' || ch == '\t');
}

static gboolean
is_not_whitespace (gunichar ch,
                   gpointer user_data)
{
  return ! is_whitespace (ch, user_data);
}

static gboolean
find_whitepace_region (const GtkTextIter *center,
                       GtkTextIter       *start,
                       GtkTextIter       *end)
{
  *start = *center;
  *end   = *center;

  if (gtk_text_iter_backward_find_char (start, is_not_whitespace, NULL, NULL))
    gtk_text_iter_forward_char (start); /* we want the first whitespace... */

  if (is_whitespace (gtk_text_iter_get_char (end), NULL))
    gtk_text_iter_forward_find_char (end, is_not_whitespace, NULL, NULL);

  return ! gtk_text_iter_equal (start, end);
}

static void
gimp_helper_delete_from_cursor (GimpTextTool  *helper,
                                   GtkDeleteType  type,
                                   gint           count)
{
  GtkTextBuffer *buffer = GTK_TEXT_BUFFER (helper->buffer);
  GtkTextIter    cursor;
  GtkTextIter    end;

  GIMP_LOG (TEXT_EDITING, "%s count = %d",
            g_enum_get_value (g_type_class_ref (GTK_TYPE_DELETE_TYPE),
                              type)->value_name,
            count);

  gimp_helper_reset_im_context (helper);

  gtk_text_buffer_get_iter_at_mark (buffer, &cursor,
                                    gtk_text_buffer_get_insert (buffer));
  end = cursor;

  switch (type)
    {
    case GTK_DELETE_CHARS:
      if (gtk_text_buffer_get_has_selection (buffer))
        {
          gtk_text_buffer_delete_selection (buffer, TRUE, TRUE);
          return;
        }
      else
        {
          gtk_text_iter_forward_cursor_positions (&end, count);
        }
      break;

    case GTK_DELETE_WORD_ENDS:
      if (count < 0)
        {
          if (! gtk_text_iter_starts_word (&cursor))
            gtk_text_iter_backward_visible_word_starts (&cursor, 1);
        }
      else if (count > 0)
        {
          if (! gtk_text_iter_ends_word (&end) &&
              ! gtk_text_iter_forward_visible_word_ends (&end, 1))
            gtk_text_iter_forward_to_line_end (&end);
        }
      break;

    case GTK_DELETE_WORDS:
      if (! gtk_text_iter_starts_word (&cursor))
        gtk_text_iter_backward_visible_word_starts (&cursor, 1);

      if (! gtk_text_iter_ends_word (&end) &&
          ! gtk_text_iter_forward_visible_word_ends (&end, 1))
        gtk_text_iter_forward_to_line_end (&end);
      break;

    case GTK_DELETE_DISPLAY_LINES:
      break;

    case GTK_DELETE_DISPLAY_LINE_ENDS:
      break;

    case GTK_DELETE_PARAGRAPH_ENDS:
      if (count < 0)
        {
          gtk_text_iter_set_line_offset (&cursor, 0);
        }
      else if (count > 0)
        {
          if (! gtk_text_iter_ends_line (&end))
            gtk_text_iter_forward_to_line_end (&end);
          else
            gtk_text_iter_forward_cursor_positions (&end, 1);
        }
      break;

    case GTK_DELETE_PARAGRAPHS:
      break;

    case GTK_DELETE_WHITESPACE:
      find_whitepace_region (&cursor, &cursor, &end);
      break;
    }

  if (! gtk_text_iter_equal (&cursor, &end))
    {
      gtk_text_buffer_delete_interactive (buffer, &cursor, &end, TRUE);
    }
}

static void
gimp_helper_backspace (GimpTextTool *helper)
{
  GtkTextBuffer *buffer = GTK_TEXT_BUFFER (helper->buffer);

  gimp_helper_reset_im_context (helper);

  if (gtk_text_buffer_get_has_selection (buffer))
    {
      gtk_text_buffer_delete_selection (buffer, TRUE, TRUE);
    }
  else
    {
      GtkTextIter cursor;

      gtk_text_buffer_get_iter_at_mark (buffer, &cursor,
                                        gtk_text_buffer_get_insert (buffer));

      gtk_text_buffer_backspace (buffer, &cursor, TRUE, TRUE);
    }
}

static void
gimp_helper_toggle_overwrite (GimpTextTool *helper)
{
  gimp_draw_tool_pause (GIMP_DRAW_TOOL (helper));

  helper->overwrite_mode = ! helper->overwrite_mode;

  gimp_draw_tool_resume (GIMP_DRAW_TOOL (helper));
}

static void
gimp_helper_select_all (GimpTextTool *helper,
                           gboolean      select)
{
  GtkTextBuffer *buffer = GTK_TEXT_BUFFER (helper->buffer);

  gimp_draw_tool_pause (GIMP_DRAW_TOOL (helper));

  if (select)
    {
      GtkTextIter start, end;

      gtk_text_buffer_get_bounds (buffer, &start, &end);
      gtk_text_buffer_select_range (buffer, &start, &end);
    }
  else
    {
      GtkTextIter cursor;

      gtk_text_buffer_get_iter_at_mark (buffer, &cursor,
					gtk_text_buffer_get_insert (buffer));
      gtk_text_buffer_move_mark_by_name (buffer, "selection_bound", &cursor);
    }

  gimp_draw_tool_resume (GIMP_DRAW_TOOL (helper));
}

static void
gimp_helper_change_size (GimpTextTool *helper,
                            gdouble       amount)
{
  GtkTextBuffer *buffer = GTK_TEXT_BUFFER (helper->buffer);
  GtkTextIter    start;
  GtkTextIter    end;

  if (! gtk_text_buffer_get_selection_bounds (buffer, &start, &end))
    {
      return;
    }

  gtk_text_iter_order (&start, &end);
  gimp_text_buffer_change_size (helper->buffer, &start, &end,
                                amount * PANGO_SCALE);
}

static void
gimp_helper_change_baseline (GimpTextTool *helper,
                                gdouble       amount)
{
  GtkTextBuffer *buffer = GTK_TEXT_BUFFER (helper->buffer);
  GtkTextIter    start;
  GtkTextIter    end;

  if (! gtk_text_buffer_get_selection_bounds (buffer, &start, &end))
    {
      gtk_text_buffer_get_iter_at_mark (buffer, &start,
                                        gtk_text_buffer_get_insert (buffer));
      gtk_text_buffer_get_end_iter (buffer, &end);
    }

  gtk_text_iter_order (&start, &end);
  gimp_text_buffer_change_baseline (helper->buffer, &start, &end,
                                    amount * PANGO_SCALE);
}

static void
gimp_helper_change_kerning (GimpTextTool *helper,
                               gdouble       amount)
{
  GtkTextBuffer *buffer = GTK_TEXT_BUFFER (helper->buffer);
  GtkTextIter    start;
  GtkTextIter    end;

  if (! gtk_text_buffer_get_selection_bounds (buffer, &start, &end))
    {
      gtk_text_buffer_get_iter_at_mark (buffer, &start,
                                        gtk_text_buffer_get_insert (buffer));
      end = start;
      gtk_text_iter_forward_char (&end);
    }

  gtk_text_iter_order (&start, &end);
  gimp_text_buffer_change_kerning (helper->buffer, &start, &end,
                                   amount * PANGO_SCALE);
}

static void
gimp_helper_options_notify (GimpTextOptions *options,
                               GParamSpec      *pspec,
                               GimpTextTool    *helper)
{
  const gchar *param_name = g_param_spec_get_name (pspec);

  if (! strcmp (param_name, "use-editor"))
    {
      if (options->use_editor)
        {
          gimp_helper_editor_dialog (helper);
        }
      else
        {
          if (helper->editor_dialog)
            gtk_widget_destroy (helper->editor_dialog);
        }
    }
}

static void
gimp_helper_editor_dialog (GimpTextTool *helper)
{
  GimpTool          *tool    = GIMP_TOOL (helper);
  GimpTextOptions   *options = GIMP_helper_GET_OPTIONS (helper);
  GimpDialogFactory *dialog_factory;
  GtkWindow         *parent  = NULL;
  gdouble            xres    = 1.0;
  gdouble            yres    = 1.0;

  if (helper->editor_dialog)
    {
      gtk_window_present (GTK_WINDOW (helper->editor_dialog));
      return;
    }

  dialog_factory = gimp_dialog_factory_get_singleton ();

  if (tool->display)
    {
      GimpDisplayShell *shell = gimp_display_get_shell (tool->display);

      parent = GTK_WINDOW (gtk_widget_get_toplevel (GTK_WIDGET (shell)));
    }

  if (helper->image)
    gimp_image_get_resolution (helper->image, &xres, &yres);

  helper->editor_dialog =
    gimp_text_options_editor_new (parent, tool->tool_info->gimp, options,
                                  gimp_dialog_factory_get_menu_factory (dialog_factory),
                                  _("GIMP Text Editor"),
                                  helper->proxy, helper->buffer,
                                  xres, yres);

  g_object_add_weak_pointer (G_OBJECT (helper->editor_dialog),
                             (gpointer) &helper->editor_dialog);

  gimp_dialog_factory_add_foreign (dialog_factory,
                                   "gimp-text-tool-dialog",
                                   helper->editor_dialog);

  g_signal_connect (helper->editor_dialog, "destroy",
                    G_CALLBACK (gimp_helper_editor_destroy),
                    helper);

  gtk_widget_show (helper->editor_dialog);
}

static void
gimp_helper_editor_destroy (GtkWidget    *dialog,
                               GimpTextTool *helper)
{
  GimpTextOptions *options = GIMP_helper_GET_OPTIONS (helper);

  g_object_set (options,
                "use-editor", FALSE,
                NULL);
}

static void
gimp_helper_enter_text (GimpTextTool *helper,
                           const gchar  *str)
{
  GtkTextBuffer *buffer = GTK_TEXT_BUFFER (helper->buffer);
  gboolean       had_selection;

  had_selection = gtk_text_buffer_get_has_selection (buffer);

  gtk_text_buffer_begin_user_action (buffer);

  gimp_helper_delete_selection (helper);

  if (! had_selection && helper->overwrite_mode && strcmp (str, "\n"))
    {
      GtkTextIter cursor;

      gtk_text_buffer_get_iter_at_mark (buffer, &cursor,
                                        gtk_text_buffer_get_insert (buffer));

      if (! gtk_text_iter_ends_line (&cursor))
        gimp_helper_delete_from_cursor (helper, GTK_DELETE_CHARS, 1);
    }

  gimp_text_buffer_insert (helper->buffer, str);

  gtk_text_buffer_end_user_action (buffer);
}

static void
gimp_helper_xy_to_iter (GimpTextTool *helper,
                           gdouble       x,
                           gdouble       y,
                           GtkTextIter  *iter)
{
  PangoLayout *layout;
  gint         offset_x;
  gint         offset_y;
  gint         index;
  gint         trailing;

  gimp_helper_ensure_layout (helper);

  gimp_text_layout_untransform_point (helper->layout, &x, &y);

  gimp_text_layout_get_offsets (helper->layout, &offset_x, &offset_y);
  x -= offset_x;
  y -= offset_y;

  layout = gimp_text_layout_get_pango_layout (helper->layout);

  pango_layout_xy_to_index (layout,
                            x * PANGO_SCALE,
                            y * PANGO_SCALE,
                            &index, &trailing);

  gimp_text_buffer_get_iter_at_index (helper->buffer, iter, index, TRUE);

  if (trailing)
    gtk_text_iter_forward_char (iter);
}

static void
gimp_helper_im_commit (GtkIMContext *context,
                          const gchar  *str,
                          GimpTextTool *helper)
{
  gimp_helper_enter_text (helper, str);
}

static void
gimp_helper_im_preedit_start (GtkIMContext *context,
                                 GimpTextTool *helper)
{
  GimpTool         *tool  = GIMP_TOOL (helper);
  GimpDisplayShell *shell = gimp_display_get_shell (tool->display);
  GtkStyle         *style = gtk_widget_get_style (shell->canvas);
  GtkWidget        *frame;
  GtkWidget        *ebox;
  PangoRectangle    cursor_rect = { 0, };
  gint              off_x, off_y;

  if (helper->text)
    gimp_helper_editor_get_cursor_rect (helper,
                                           helper->overwrite_mode,
                                           &cursor_rect);

  g_object_get (helper, "x1", &off_x, "y1", &off_y, NULL);

  helper->preedit_overlay = gtk_frame_new (NULL);
  gtk_frame_set_shadow_type (GTK_FRAME (helper->preedit_overlay),
                             GTK_SHADOW_OUT);
  gimp_display_shell_add_overlay (shell,
                                  helper->preedit_overlay,
                                  cursor_rect.x + off_x,
                                  cursor_rect.y + off_y,
                                  GIMP_HANDLE_ANCHOR_NORTH_WEST, 0, 0);
  gimp_overlay_box_set_child_opacity (GIMP_OVERLAY_BOX (shell->canvas),
                                      helper->preedit_overlay, 0.7);
  gtk_widget_show (helper->preedit_overlay);

  frame = gtk_frame_new (NULL);
  gtk_frame_set_shadow_type (GTK_FRAME (frame), GTK_SHADOW_IN);
  gtk_container_add (GTK_CONTAINER (helper->preedit_overlay), frame);
  gtk_widget_show (frame);

  ebox = gtk_event_box_new ();
  gtk_widget_modify_bg (ebox, GTK_STATE_NORMAL,
                        &style->base[GTK_STATE_NORMAL]);
  gtk_container_add (GTK_CONTAINER (frame), ebox);
  gtk_widget_show (ebox);

  helper->preedit_label = gtk_label_new (NULL);
  gtk_widget_modify_bg (helper->preedit_label, GTK_STATE_NORMAL,
                        &style->text[GTK_STATE_NORMAL]);
  gtk_misc_set_padding (GTK_MISC (helper->preedit_label), 2, 2);
  gtk_container_add (GTK_CONTAINER (ebox), helper->preedit_label);
  gtk_widget_show (helper->preedit_label);

  gimp_helper_editor_update_im_rect (helper);
}

static void
gimp_helper_im_preedit_end (GtkIMContext *context,
                               GimpTextTool *helper)
{
  if (helper->preedit_overlay)
    {
      gtk_widget_destroy (helper->preedit_overlay);
      helper->preedit_overlay = NULL;
      helper->preedit_label   = NULL;
    }
}

static void
gimp_helper_im_preedit_changed (GtkIMContext *context,
                                   GimpTextTool *helper)
{
  if (helper->preedit_string)
    g_free (helper->preedit_string);

  gtk_im_context_get_preedit_string (context,
                                     &helper->preedit_string, NULL,
                                     &helper->preedit_cursor);

  if (helper->preedit_label)
    gtk_label_set_text (GTK_LABEL (helper->preedit_label),
                        helper->preedit_string);
}
