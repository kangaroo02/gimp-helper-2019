/* GIMP - The GNU Image Manipulation Program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * GimpHelper
 * Copyright (C) 2002-2003  Sven Neumann <sven@gimp.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __GIMP_HELPER_H__
#define __GIMP_HELPER_H__


#include "core/gimpobject.h"


#define GIMP_TYPE_HELPER            (gimp_helper_get_type ())
#define GIMP_HELPER(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), GIMP_TYPE_HELPER, GimpHelper))
#define GIMP_HELPER_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), GIMP_TYPE_HELPER, GimpHelperClass))
#define GIMP_IS_HELPER(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GIMP_TYPE_HELPER))
#define GIMP_IS_HELPER_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), GIMP_TYPE_HELPER))
#define GIMP_HELPER_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), GIMP_TYPE_HELPER, GimpHelperClass))


typedef struct _GimpHelperClass  GimpHelperClass;

struct _GimpHelper
{
  GimpObject             parent_instance;

  gchar                 *text;
  gchar                 *markup;
  gchar                 *font;
  GimpUnit               unit;
  gdouble                font_size;
  gboolean               antialias;
  GimpHelperHintStyle      hint_style;
  gboolean               kerning;
  gchar                 *language;
  GimpHelperDirection      base_dir;
  GimpRGB                color;
  GimpHelperOutline        outline;
  GimpHelperJustification  justify;
  gdouble                indent;
  gdouble                line_spacing;
  gdouble                letter_spacing;
  GimpHelperBoxMode        box_mode;
  gdouble                box_width;
  gdouble                box_height;
  GimpUnit               box_unit;
  GimpMatrix2            transformation;
  gdouble                offset_x;
  gdouble                offset_y;

  gdouble                border;
};

struct _GimpHelperClass
{
  GimpObjectClass        parent_class;

  void (* changed) (GimpHelper *text);
};


GType  gimp_helper_get_type           (void) G_GNUC_CONST;

void   gimp_helper_get_transformation (GimpHelper    *text,
                                     GimpMatrix3 *matrix);


#endif /* __GIMP_HELPER_H__ */
