/* GIMP - The GNU Image Manipulation Program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * GimpHelper
 * Copyright (C) 2002-2003  Sven Neumann <sven@gimp.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __TEXT_TYPES_H__
#define __TEXT_TYPES_H__

#include "core/core-types.h"

#include "text/text-enums.h"


typedef struct _GimpFont       GimpFont;
typedef struct _GimpFontList   GimpFontList;
typedef struct _GimpHelper       GimpHelper;
typedef struct _GimpHelperLayer  GimpHelperLayer;
typedef struct _GimpHelperLayout GimpHelperLayout;
typedef struct _GimpHelperUndo   GimpHelperUndo;


#endif /* __TEXT_TYPES_H__ */
