/* GIMP - The GNU Image Manipulation Program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * GimpHelper
 * Copyright (C) 2003  Sven Neumann <sven@gimp.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __GIMP_HELPER_PARASITE_H__
#define __GIMP_HELPER_PARASITE_H__


const gchar  * gimp_helper_parasite_name          (void) G_GNUC_CONST;
GimpParasite * gimp_helper_to_parasite            (const GimpHelper      *text);
GimpHelper     * gimp_helper_from_parasite          (const GimpParasite  *parasite,
                                                 GError             **error);

const gchar  * gimp_helper_gdyntext_parasite_name (void) G_GNUC_CONST;
GimpHelper     * gimp_helper_from_gdyntext_parasite (const GimpParasite  *parasite);


#endif /* __GIMP_HELPER_PARASITE_H__ */
