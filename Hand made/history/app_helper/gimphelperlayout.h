/* GIMP - The GNU Image Manipulation Program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * GimpHelper
 * Copyright (C) 2002-2003  Sven Neumann <sven@gimp.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __GIMP_HELPER_LAYOUT_H__
#define __GIMP_HELPER_LAYOUT_H__


#define GIMP_TYPE_HELPER_LAYOUT    (gimp_helper_layout_get_type ())
#define GIMP_HELPER_LAYOUT(obj)    (G_TYPE_CHECK_INSTANCE_CAST ((obj), GIMP_TYPE_HELPER_LAYOUT, GimpHelperLayout))
#define GIMP_IS_HELPER_LAYOUT(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GIMP_TYPE_HELPER_LAYOUT))


typedef struct _GimpHelperLayoutClass GimpHelperLayoutClass;

struct _GimpHelperLayoutClass
{
  GObjectClass   parent_class;
};


GType            gimp_helper_layout_get_type             (void) G_GNUC_CONST;

GimpHelperLayout * gimp_helper_layout_new                  (GimpHelper       *text,
                                                        gdouble         xres,
                                                        gdouble         yres,
                                                        GError        **error);
gboolean         gimp_helper_layout_get_size             (GimpHelperLayout *layout,
                                                        gint           *width,
                                                        gint           *heigth);
void             gimp_helper_layout_get_offsets          (GimpHelperLayout *layout,
                                                        gint           *x,
                                                        gint           *y);
void             gimp_helper_layout_get_resolution       (GimpHelperLayout *layout,
                                                        gdouble        *xres,
                                                        gdouble        *yres);

GimpHelper       * gimp_helper_layout_get_text             (GimpHelperLayout *layout);
PangoLayout    * gimp_helper_layout_get_pango_layout     (GimpHelperLayout *layout);

void             gimp_helper_layout_get_transform        (GimpHelperLayout *layout,
                                                        cairo_matrix_t *matrix);

void             gimp_helper_layout_transform_rect       (GimpHelperLayout *layout,
                                                        PangoRectangle *rect);
void             gimp_helper_layout_transform_point      (GimpHelperLayout *layout,
                                                        gdouble        *x,
                                                        gdouble        *y);
void             gimp_helper_layout_transform_distance   (GimpHelperLayout *layout,
                                                        gdouble        *x,
                                                        gdouble        *y);

void             gimp_helper_layout_untransform_rect     (GimpHelperLayout *layout,
                                                        PangoRectangle *rect);
void             gimp_helper_layout_untransform_point    (GimpHelperLayout *layout,
                                                        gdouble        *x,
                                                        gdouble        *y);
void             gimp_helper_layout_untransform_distance (GimpHelperLayout *layout,
                                                        gdouble        *x,
                                                        gdouble        *y);


#endif /* __GIMP_HELPER_LAYOUT_H__ */
